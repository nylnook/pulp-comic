Pulp Comic, as a plugin for Wordpress
==============

An open-source viewer for displaying comics online, developed for the story [Terms of Service](http://projects.aljazeera.com/2014/terms-of-service) by [Michael Keller](https://mhkeller.com/). Pulp was forked into [Pulp2](https://github.com/mhkeller/pulp2) to continue development and support after the closure of Al Jazeera America. And [nylnook](https://nylnook.art/) is integrating it to Wordpress as a plugin.

Features
-------
#### From Pulp:
- Mobile responsive.
	- On large screens, you'll see a two-page spread.
	- On medium-width screens, you'll see a single page
	- On mobile screens (or any browser width that's smaller than the normal image width) Pulp will navigate panel by panel
- Swipe-able on mobile.
- Fullscreen mode.
- Endnotes for including hyperlinks.
- The final page in the comic takes arbitrary text so you can link out or otherwise give instructions for readers to do when they're done.

#### Added by the Wordpress plugin:
- Integrate into Wordpres creating a new Custom Post Type : the Pulp Comics.
- Pulp Comic settings inside the Wordpress settings with additonal options.
- Some settings are comic specific with additonal options.
- Add Internationalization : plugin translated to English and French, contributions welcome.
- If you translated the pages, comics inside the website can be several languages with the MultilingualPress plugin.
- Reuse the blog logo for whitelabeling.
- Configurable share buttons.
- Add configurable support button (Patreon/Tipeee/Liberapay).
- Add support for series of comics, each comic become an episode.


Requirements / assumptions
-------
- You have one image for every page in your comic and each page is made up of panels.
- You have a cover image that should be displayed on its own page.


Install the plugin
-------
Dowload this repository and put the `pulp-comic` directory into `/wp-content/plugins/` on your website FTP, then enable it in the Wordpress admin.

How to use
-------
1. Layout your comic pages with [Pulp Press](http://ajam.github.io/pulp-press)
2. Create a new Pulp Comic in Wordpress and copy the new *slug* of this special post, for example `test-comic`
3. Depending on your Pulp settings, in your FTP, go to your `/wp-content/uploads/` and create a new `/pulp-comic/` folder if it doesn't already exist, and then a new folder with the same *slug* name than the newly created comic in Wordpress, for example `test-comic`
4. Upload your comic created with Pulp Press in this new folder: it must have the same directory tree (at least a `/data/pages.json` and a `/imgs/pages/page-1.jpg`)
5. You should have this tree
		- `test-comic`
			- `data`
				- `pages.json`
			- `imgs`
				- `pages`
					- `page-1.jpg`
					- `page-2.jpg`
					- `page-n.jpg`
6. Test to see your Pulp comic in Wordpress : if you have done everything right, you should see your pages surrounded by the Pulp viewer!

Version
-------
0.1 alpha, fresh initial release : Pulp Comic is working, but Pulp Press is not integrated

Tested with
-------
Wordpress 5 multisite

Upstream repositories
-------
Based on the work of [Michael Keller](https://mhkeller.com/).
- [Pulp 2](https://github.com/mhkeller/pulp2)
- [Pulp press 2](https://github.com/mhkeller/pulp-press2)

Contributors
-------
- [Camille "nylnook" Bissuel](http://nylnook.art)

TODO
-------
- Intergate Pulp Press fully, may need a rewrite
- Allow to create chapters inside comics : [Example](https://pellichi.fr/manga/mat_met_2/#66)
- Allow to use comics as a Guttenberg (the new Wordpress editor) block into other posts
- Add a demo comic by default
- Add a tutorial and improve explanations

License
--------
Except if stated otherwise, the whole code is released under MIT license
