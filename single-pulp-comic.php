<?php
/**
 * The template for displaying all single comic
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Pulp_Comic
 */

// Store the pulp comic settings option array in a variable to reuse it easily
$pulp_comic_settings = get_option('pulp_comic_settings');

// Also get post meta (or post specific settings)
$pulp_comic_meta = get_post_meta( $post->ID, 'pulp_comic_meta', false );

// Get presentation post link or default to blog url
if (!empty($pulp_comic_meta[0]['presentation_link'])) {
	$presentation_post_url = $pulp_comic_meta[0]['presentation_link'];
}	else {
	$presentation_post_url = get_bloginfo('url');
}

// Start the loop
while ( have_posts() ) : the_post(); ?>
<!DOCTYPE html>
<html lang="<?php echo $lang=get_bloginfo("language"); ?>">

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

		<!-- YOU CAN ADD .ICO ICONS HERE -->
	  <link rel="icon" href="<?php echo get_site_icon_url(32); ?>"/>
		<link rel="icon" href="<?php echo get_site_icon_url(192); ?>"/>
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_site_icon_url(180); ?>">
		<meta name="msapplication-TileImage" content="<?php echo get_site_icon_url(270); ?>" />

		<title><?php the_title(); ?></title>

		<?php if ( file_exists( plugin_dir_path( __FILE__ ) . '/css/thirdparty/reset.css' ) ) : ?>
	 		<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) . 'css/thirdparty/reset.css' ?>">
		<?php endif;?>
		<?php if ( file_exists( plugin_dir_path( __FILE__ ) . '/css/thirdparty/pulp-icon.css' ) ) : ?>
			<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) . 'css/thirdparty/pulp-icon.css' ?>">
		<?php endif;?>
		<?php if ( file_exists( plugin_dir_path( __FILE__ ) . '/css/styles.css' ) ) : ?>
			<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) . 'css/styles.css' ?>">
		<?php endif;?>

	 	<!-- WHAT APPEARS FOR SEARCH ENGINES -->
    <meta name="robots" content="noodp">
		<?php if (!empty($pulp_comic_meta[0]['keywords'])) : ?>
    	<meta name="keywords"  content="<?php echo $pulp_comic_meta[0]['keywords'] ?>" />
		<?php endif;?>
    <meta name="description" content="<?php echo $pulp_comic_meta[0]['description'] ?>"/>

	 	<!-- DON'T FORGET TO FILL OUT OpenGraph FIELDS -->
	 	<!-- http://ogp.me/ -->
		<meta property="og:title" content="<?php the_title(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>">
		<meta property="og:site_name" content="<?php echo get_bloginfo('name'); ?>">
		<meta property="og:description" content="<?php echo $pulp_comic_meta[0]['description'] ?>">

    <!-- TWITTER TAGS -->
    <!-- https://dev.twitter.com/cards/markup -->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@<?php echo $pulp_comic_settings['twitter_account']; ?>">
		<meta name="twitter:title" content="<?php the_title(); ?>">
		<meta name="twitter:creator" content="@<?php echo $pulp_comic_settings['twitter_account']; ?>">
		<meta name="twitter:image:src" content="<?php the_post_thumbnail_url(); ?>">
		<!-- at least 280px x 150px for Twitter (image must be < 1MB) -->
		<meta name="twitter:description" content="<?php echo $pulp_comic_meta[0]['description'] ?>">

	</head>
	<body>

		<!--[if lt IE 10]>
				<p class="browsehappy">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
		<![endif]-->

		<noscript>
			<p><?php _e( 'It appears you have JavaScript disabled.', 'pulp-comic' ) ?></p>
			<?php if (!empty($pulp_comic_meta[0]['pdf_link'])) : ?>
				<p><?php _e( 'View the PDF version:', 'pulp-comic' ) ?><a href="<?php echo $pulp_comic_meta[0]['pdf_link'] ?>"><?php the_title(); ?></a></p>
			<?php endif;?>
		</noscript>

		<div id="side-drawer-container" class="mobile-scrollable">
		<div id="side-drawer-content">
			<?php if ($pulp_comic_settings['twitter_check'] == "1" or $pulp_comic_settings['facebook_check'] == "1" or $pulp_comic_settings['gplus_check'] == "1" or $pulp_comic_settings['reddit_check'] == "1" or $pulp_comic_settings['diaspora_check'] == "1" or $pulp_comic_settings['mastodon_check'] == "1") : ?>
			<div class="drawer-section-container" data-which="share">
				<ul>
					<?php if ($pulp_comic_settings['twitter_check'] == "1") : ?>
						<li class="pulp-icon-twitter social-btn" data-which="twitter"></li>
					<?php endif;?>
					<?php if ($pulp_comic_settings['facebook_check'] == "1") : ?>
						<li class="pulp-icon-facebook social-btn" data-which="facebook"></li>
					<?php endif;?>
					<?php if ($pulp_comic_settings['gplus_check'] == "1") : ?>
						<li class="pulp-icon-gplus social-btn" data-which="gplus"></li>
					<?php endif;?>
					<?php if ($pulp_comic_settings['reddit_check'] == "1") : ?>
						<li class="pulp-icon-reddit social-btn" data-which="reddit"></li>
					<?php endif;?>
					<?php if ($pulp_comic_settings['diaspora_check'] == "1") : ?>
						<a href="javascript:;" onclick="window.open('https://share.diasporafoundation.org/?url='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title),'das','location=no,links=no,scrollbars=no,toolbar=no,width=620,height=550'); return false;" rel="nofollow" target="_blank">
						<li class="pulp-icon-diaspora social-btn" data-which="diaspora"></li>
						</a>
					<?php endif;?>
					<?php if ($pulp_comic_settings['mastodon_check'] == "1") : ?>
						<a href="javascript:;" onclick="window.open('web+mastodon://share?text=<?php echo $pulp_comic_settings['twitter_text']?>','das','width=400,height=400,resizable=no,menubar=no,status=no,scrollbars=yes'); return false;" rel="nofollow" target="_blank">
							<li class="pulp-icon-mastodon social-btn" data-which="mastodon"></li>
						</a>
					<?php endif;?>
				</ul>
			</div>
			<?php endif;?>
			<?php if ($pulp_comic_settings['patreon_check'] == "1" or $pulp_comic_settings['tipeee_check'] == "1" or $pulp_comic_settings['liberapay_check'] == "1") : ?>
			<div class="drawer-section-container" data-which="support">
				<ul>
					<?php if ($pulp_comic_settings['patreon_check'] == "1") : ?>
						<a href="https://www.patreon.com/<?php echo $pulp_comic_settings['patreon_account']?>" target="_blank">
							<li class="pulp-icon-patreon social-btn" data-which="patreon"></li>
						</a>
					<?php endif;?>
					<?php if ($pulp_comic_settings['tipeee_check'] == "1") : ?>
						<a href="https://www.tipeee.com/<?php echo $pulp_comic_settings['tipeee_account']?>" target="_blank">
							<li class="pulp-icon-tipeee social-btn" data-which="tipeee"></li>
						</a>
					<?php endif;?>
					<?php if ($pulp_comic_settings['liberapay_check'] == "1") : ?>
						<a href="https://liberapay.com/<?php echo $pulp_comic_settings['liberapay_account']?>" target="_blank">
							<li class="pulp-icon-liberapay social-btn" data-which="liberapay"></li>
						</a>
					<?php endif;?>
				</ul>
			</div>
			<?php endif;?>
			<div class="drawer-section-container" data-section="pub-date">
				<div class="section-subtitle"><?php _e( 'Published:', 'pulp-comic' ); ?><span class="pub-date-js"></span></div>
			</div>
			<?php if (!empty($pulp_comic_meta[0]['google_play_link']) or !empty($pulp_comic_meta[0]['ibooks_link']) or !empty($pulp_comic_meta[0]['epub_link']) or !empty($pulp_comic_meta[0]['pdf_link'])) : ?>
			<div class="drawer-section-container" data-section="downloads">
				<div class="section-title"><?php _e( 'Download', 'pulp-comic' ); ?></div>
				<ul>
					<?php if (!empty($pulp_comic_meta[0]['google_play_link'])) : ?>
						<a data-which="googleplay" href="<?php echo $pulp_comic_meta[0]['google_play_link'] ?>" target="_blank"><li><?php _e( 'On Google Play...', 'pulp-comic' ); ?></li></a>
					<?php endif;?>
					<?php if (!empty($pulp_comic_meta[0]['ibooks_link'])) : ?>
						<a data-which="ibooks" href="<?php echo $pulp_comic_meta[0]['ibooks_link'] ?>" target="_blank"><li><?php _e( 'On iBooks...', 'pulp-comic' ); ?></li></a>
					<?php endif;?>
					<?php if (!empty($pulp_comic_meta[0]['epub_link'])) : ?>
						<a data-which="epub" href="<?php echo $pulp_comic_meta[0]['epub_link'] ?>" target="_blank"><li><?php _e( 'As ePub...', 'pulp-comic' ); ?></li></a>
					<?php endif;?>
					<?php if (!empty($pulp_comic_meta[0]['pdf_link'])) : ?>
						<a data-which="pdf" href="<?php echo $pulp_comic_meta[0]['pdf_link'] ?>" target="_blank"><li><?php _e( 'As PDF...', 'pulp-comic' ); ?></li></a>
					<?php endif;?>
				</ul>
			</div>
			<?php endif;?>
			<?php if (!empty($pulp_comic_meta[0]['presentation_link'])) : ?>
			<div class="drawer-section-container">
				<div class="section-title"><?php _e( "Let's talk!", 'pulp-comic' ); ?></div>
				<ul>
					<a href="<?php echo $presentation_post_url; ?>" target="_blank"><li><?php _e( 'Comments', 'pulp-comic' ); ?></li></a>
				</ul>
			</div>
			<?php endif;?>
			<?php if (!empty(get_the_term_list($post->ID, 'pulp_comic_series'))) : ?>
				<div class="drawer-section-container" data-section="series">
					<div class="section-title"><?php _e( 'Series', 'pulp-comic' ); ?></div>
					<?php echo get_the_term_list( $post->ID, 'pulp_comic_series', '<ul><li>', '</li><li>', '</li></ul>' ); ?>
					<?php $prev_post = get_adjacent_post( true, '', true, 'pulp_comic_series' ); ?>
					<?php $next_post = get_adjacent_post( true, '', false, 'pulp_comic_series' ); ?>
					<?php if ( is_a( $prev_post, 'WP_Post' ) or is_a( $next_post, 'WP_Post' )  ) { ?>
							<ul>
						<?php if ( is_a( $prev_post, 'WP_Post' ) ) { ?>
							<li>
									<a rel="prev" href="<?php echo get_permalink($prev_post->ID); ?>">
										<?php _e( 'Previous episode: ', 'pulp-comic' ); ?><?php echo get_the_title( $prev_post->ID ); ?>
									</a>
							</li>
						<?php } ?>
						<?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
							<li>
									<a rel="next" href="<?php echo get_permalink($next_post->ID); ?>">
										<?php _e( 'Next episode: ', 'pulp-comic' ); ?><?php echo get_the_title( $next_post->ID ); ?>
									</a>
							</li>
						<?php } ?>
					<?php } ?>
				</div>
			<?php endif;?>
			<?php if (!empty($pulp_comic_meta[0]['show_endnotes'])) : ?>
			<div class="drawer-section-container">
				<div class="section-title"><?php _e( 'Notes', 'pulp-comic' ); ?></div>
				<ul class="endnotes" data-which="mobile">
				</ul>
			</div>
			<?php endif;?>
		</div>
	</div>
	<div id="main-content-wrapper">
		<div id="header">
			<ul class="header-group mobile">
				<li class="header-item-container" data-btn="hamburger" data-action="drawer">
					<div class="header-item header-btn">
						<div class="pulp-icon-menu header-btn-icon"></div>
					</div>
				</li>
			</ul>
			<ul class="header-group desktop">
				<?php if (!empty($pulp_comic_meta[0]['show_endnotes'])) : ?>
				<li class="header-item-container tooltipped" data-btn="endnotes" data-action="drawer" aria-label="<?php _e( 'Notes', 'pulp-comic' ); ?>">
					<div class="header-item header-btn">
						<div class="pulp-icon-dot-3 header-btn-icon"></div>
					</div>
				</li>
				<?php endif;?>
				<li class="header-item-container tooltipped mobile-hide" data-btn="fullscreen" aria-label="<?php _e( 'Fullscreen', 'pulp-comic' ); ?>">
					<div class="header-item header-btn">
						<div class="pulp-icon-resize-full header-btn-icon"></div>
					</div>
				</li>
				<?php if ($pulp_comic_settings['zoom_mode'] == "desktop-hover") : ?>
				<li class="header-item-container tooltipped mobile-hide" data-btn="enable-zoom" aria-label="<?php _e( 'Toggle hover zoom', 'pulp-comic' ); ?>">
					<div class="header-item header-btn">
						<div class="pulp-icon-zoom-in header-btn-icon"></div>
					</div>
				</li>
				<?php endif;?>
				<?php if (!empty($pulp_comic_meta[0]['google_play_link']) or !empty($pulp_comic_meta[0]['ibooks_link']) or !empty($pulp_comic_meta[0]['epub_link']) or !empty($pulp_comic_meta[0]['pdf_link'])) : ?>
				<li class="header-item-container dropdowned" data-btn="export">
					<div class="header-item header-btn">
						<span class="pulp-icon-download-cloud header-btn-icon"></span>
					</div>
					<ul class="btn-dropdown" data-which="alt-formats">
						<?php if (!empty($pulp_comic_meta[0]['google_play_link'])) : ?>
							<a data-which="googleplay" href="<?php echo $pulp_comic_meta[0]['google_play_link'] ?>" target="_blank"><li><?php _e( 'On Google Play...', 'pulp-comic' ); ?></li></a>
						<?php endif;?>
						<?php if (!empty($pulp_comic_meta[0]['ibooks_link'])) : ?>
							<a data-which="ibooks" href="<?php echo $pulp_comic_meta[0]['ibooks_link'] ?>" target="_blank"><li><?php _e( 'On iBooks...', 'pulp-comic' ); ?></li></a>
						<?php endif;?>
						<?php if (!empty($pulp_comic_meta[0]['epub_link'])) : ?>
							<a data-which="epub" href="<?php echo $pulp_comic_meta[0]['epub_link'] ?>" target="_blank"><li><?php _e( 'As ePub...', 'pulp-comic' ); ?></li></a>
						<?php endif;?>
						<?php if (!empty($pulp_comic_meta[0]['pdf_link'])) : ?>
							<a data-which="pdf" href="<?php echo $pulp_comic_meta[0]['pdf_link'] ?>" target="_blank"><li><?php _e( 'As PDF...', 'pulp-comic' ); ?></li></a>
						<?php endif;?>
					</ul>
				</li>
				<?php endif;?>
				<?php if (!empty($pulp_comic_meta[0]['presentation_link'])) : ?>
				<li class="header-item-container tooltipped" data-btn="comment" aria-label="<?php _e( 'Comments', 'pulp-comic' ); ?>">
					<div class="header-item header-btn">
						<a href="<?php echo $presentation_post_url; ?>" target="_blank"><div class="pulp-icon-chat header-btn-icon"></div></a>
					</div>
				</li>
				<?php endif;?>
				<?php if ($pulp_comic_settings['twitter_check'] == "1" or $pulp_comic_settings['facebook_check'] == "1" or $pulp_comic_settings['gplus_check'] == "1" or $pulp_comic_settings['reddit_check'] == "1" or $pulp_comic_settings['diaspora_check'] == "1" or $pulp_comic_settings['mastodon_check'] == "1") : ?>
				<li class="header-item-container dropdowned" data-btn="share">
					<div class="header-item header-btn">
						<div class="pulp-icon-share header-btn-icon"></div>
					</div>
					<ul class="btn-dropdown" data-which="share">
						<?php if ($pulp_comic_settings['twitter_check'] == "1") : ?>
							<li class="pulp-icon-twitter social-btn" data-which="twitter"></li>
						<?php endif;?>
						<?php if ($pulp_comic_settings['facebook_check'] == "1") : ?>
							<li class="pulp-icon-facebook social-btn" data-which="facebook"></li>
						<?php endif;?>
						<?php if ($pulp_comic_settings['gplus_check'] == "1") : ?>
							<li class="pulp-icon-gplus social-btn" data-which="gplus"></li>
						<?php endif;?>
						<?php if ($pulp_comic_settings['reddit_check'] == "1") : ?>
							<li class="pulp-icon-reddit social-btn" data-which="reddit"></li>
						<?php endif;?>
						<?php if ($pulp_comic_settings['diaspora_check'] == "1") : ?>
							<a href="javascript:;" onclick="window.open('https://share.diasporafoundation.org/?url='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title),'das','location=no,links=no,scrollbars=no,toolbar=no,width=620,height=550'); return false;" rel="nofollow" target="_blank">
								<li class="pulp-icon-diaspora social-btn" data-which="diaspora"></li>
							</a>
						<?php endif;?>
						<?php if ($pulp_comic_settings['mastodon_check'] == "1") : ?>
							<a href="javascript:;" onclick="window.open('web+mastodon://share?text=<?php echo $pulp_comic_settings['twitter_text']?>','das','width=400,height=400,resizable=no,menubar=no,status=no,scrollbars=yes'); return false;" rel="nofollow" target="_blank">
								<li class="pulp-icon-mastodon social-btn" data-which="mastodon"></li>
							</a>
						<?php endif;?>
					</ul>
				</li>
				<?php endif;?>
				<?php if ($pulp_comic_settings['patreon_check'] == "1" or $pulp_comic_settings['tipeee_check'] == "1" or $pulp_comic_settings['liberapay_check'] == "1") : ?>
				<li class="header-item-container dropdowned" data-btn="support">
					<div class="header-item header-btn">
						<div class="pulp-icon-support header-btn-icon"></div>
					</div>
					<ul class="btn-dropdown" data-which="support">
						<?php if ($pulp_comic_settings['patreon_check'] == "1") : ?>
							<a href="https://www.patreon.com/<?php echo $pulp_comic_settings['patreon_account']?>" target="_blank">
								<li class="pulp-icon-patreon social-btn" data-which="patreon"></li>
							</a>
						<?php endif;?>
						<?php if ($pulp_comic_settings['tipeee_check'] == "1") : ?>
							<a href="https://www.tipeee.com/<?php echo $pulp_comic_settings['tipeee_account']?>" target="_blank">
								<li class="pulp-icon-tipeee social-btn" data-which="tipeee"></li>
							</a>
						<?php endif;?>
						<?php if ($pulp_comic_settings['liberapay_check'] == "1") : ?>
							<a href="https://liberapay.com/<?php echo $pulp_comic_settings['liberapay_account']?>" target="_blank">
								<li class="pulp-icon-liberapay social-btn" data-which="liberapay"></li>
							</a>
						<?php endif;?>
					</ul>
				</li>
				<?php endif;?>
			</ul>
			<ul class="header-group">
				<li class="header-item-container tooltipped-i"  aria-label="<?php _e( "'Return' to navigate. 'Esc' to clear.", 'pulp-comic' ); ?>">
					<div class="header-item" data-which="page-number">
						<div class="header-text">
							<?php _e( 'Page', 'pulp-comic' ); ?> <input min="1" type="number" id="page-number-input"/> / <span id="pages-max"></span>
						</div>
					</div>
			</ul>
			<?php if (!empty(get_the_term_list($post->ID, 'pulp_comic_series'))) : ?>
			<ul class="header-group desktop">
				<li class="header-item-container dropdowned" data-btn="series">
					<div class="header-item header-btn">
						<span class="pulp-icon-series header-btn-icon"></span>
					</div>
					<?php echo get_the_term_list( $post->ID, 'pulp_comic_series', '<ul class="btn-dropdown" data-which="alt-formats"><li>', '</li><li>', '</li></ul>' ); ?>
				</li>
				<?php $prev_post = get_adjacent_post( true, '', true, 'pulp_comic_series' ); ?>
				<?php if ( is_a( $prev_post, 'WP_Post' ) ) { ?>
					<li class="header-item-container tooltipped" data-btn="previous" aria-label="<?php _e( 'Previous episode: ', 'pulp-comic' ); ?><?php echo get_the_title( $prev_post->ID ); ?>">
						<div class="header-item header-btn">
							<a rel="prev" href="<?php echo get_permalink($prev_post->ID); ?>">
								<div class="pulp-icon-angle-left header-btn-icon"></div>
							</a>
						</div>
					</li>
				<?php } ?>
				<?php $next_post = get_adjacent_post( true, '', false, 'pulp_comic_series' ); ?>
				<?php if ( is_a( $next_post, 'WP_Post' ) ) {  ?>
					<li class="header-item-container tooltipped" data-btn="next" aria-label="<?php _e( 'Next episode: ', 'pulp-comic' ); ?><?php echo get_the_title( $next_post->ID ); ?>">
						<div class="header-item header-btn">
							<a rel="next" href="<?php echo get_permalink($next_post->ID); ?>">
								<div class="pulp-icon-angle-right header-btn-icon"></div>
							</a>
						</div>
					</li>
				<?php } ?>
			</ul>
			<?php endif;?>
			<ul class="header-group desktop">
				<li class="header-item-container">
					<div class="header-item" data-which="pub-date">
						<div class="header-text pub-date-js"></div>
					</div>
				</li>
			</ul>
		</div>

		<div id="desktop-drawer-container">
			<div id="desktop-drawer-content">
				<div class="section-title"><?php _e( 'Endnotes', 'pulp-comic' ); ?>
					<!-- <span class="section-title-pointer"> (page/panel)</span> -->
					<span class="section-title-close" data-action="drawer"><?php _e( 'close', 'pulp-comic' ); ?></span>
				</div>
				<ul class="endnotes" data-which="desktop">
				</ul>
			</div>
		</div>

		<div id="btns">
			<div class="main-nav-btn-container" data-dir="prev"><div class="main-nav-btn"><div class="btn-text pulp-icon-angle-left"></div></div></div><div class="main-nav-btn-container" data-dir="next"><div class="main-nav-btn"><div class="btn-text pulp-icon-angle-right"></div><div class="nav-helpers"><div class="nav-helpers-child" data-mode="intro"><?php _e( 'Use arrow keys ', 'pulp-comic' ); ?><div class="nav-helper-arrows"><span>&#8592;</span><span>&#8594;</span></div><?php _e( ' or click to navigate.', 'pulp-comic' ); ?></div><div class="nav-helpers-child" data-mode="expand"><?php _e( 'Expand window to see more pages', 'pulp-comic' ); ?><br/><img src="<?php echo plugins_url(); ?>/pulp-comic/imgs/assets/horizontal-resize.svg"/></div></div></div></div>
		</div>
		<div id="side-drawer-handle"></div>
		<div id="pages-wrapper">
			<div id="pages"></div>
		</div>

	</div>

	<!-- Send Pulp Comic Plugin settings to scripts -->
	<script>
		<?php
		// Retreive the comic slug to reuse in the path
		$comic_slug = basename(get_permalink());
		// Define comic files url
		$comic_url = $pulp_comic_settings['upload_url'] . $comic_slug . '/';
		?>
		// Send this comic files url to javascript
		var comicUrl = '<?php echo $comic_url; ?>';
		//console.log( comicUrl );

		var pulp_settings = {
			pubDate: '<?php the_date(); ?>',
			imgFormat: '<?php echo $pulp_comic_meta[0]['file_format']; ?>',
			whitelabel: {
				files: {
					js: []
				},
				logo: "<a href='<?php echo $presentation_post_url; ?>' title='<?php _e( 'Back to website', 'pulp-comic' ); ?>' ><img src='<?php echo get_site_icon_url(32); ?>' alt='<?php echo get_bloginfo('name') . ' ' . __( 'logo', 'pulp-comic' ); ?>'/></a>"
			},
			panelZoomMode: '<?php echo $pulp_comic_settings['zoom_mode']; ?>',
			desktopHoverZoomOptions: {
				scale: <?php echo $pulp_comic_settings['zoom_scale']; ?>,
				fit: <?php echo $pulp_comic_settings['zoom_fit']; ?>,
				padding: <?php echo $pulp_comic_settings['zoom_padding']; ?>,
				zoomInDelay: "<?php echo $pulp_comic_settings['zoom_in_delay']; ?>",
				zoomOutDelay: "<?php echo $pulp_comic_settings['zoom_out_delay']; ?>",
				zoomInSpeed: "<?php echo $pulp_comic_settings['zoom_in_speed']; ?>",
				zoomOutSpeed: "<?php echo $pulp_comic_settings['zoom_out_speed']; ?>",
				mouseFollowSpeed: "<?php echo $pulp_comic_settings['zoom_mouse_follow_speed']; ?>"
			},
			lazyLoadExtent: <?php echo $pulp_comic_settings['lazy_load_extent']; ?>,
			transitionDuration: <?php echo $pulp_comic_settings['transition_duration']; ?>,
			singlePageWidthLimit: <?php echo $pulp_comic_settings['single_page_width_limit']; ?>,
			gutterWidth: '<?php echo $pulp_comic_settings['gutter_width']; ?>',
			drawerTransitionDuration: <?php echo $pulp_comic_settings['drawer_transition_duration']; ?>,
			social: {
				twitter_text: '<?php echo $pulp_comic_settings['twitter_text']; ?>',
				twitter_account: '<?php echo $pulp_comic_settings['twitter_account']; ?>',
				fb_text: '<?php echo $pulp_comic_settings['fb_text']; ?>',
				promo_img_url: '<?php the_post_thumbnail_url(); ?>',
				fb_app_id: '<?php echo $pulp_comic_settings['fb_app_id']; ?>'
			},
			requireStartOnFirstPage: <?php echo $pulp_comic_settings['require_start_on_first_page']; ?>
		}
	</script>

	<!-- Endnotes template, split on the first color since that will be our page panel convention -->
  <script type="text/template" id="endnote-template">
    <li class="endnote"><span class="endnote-number"><%= endnote.number %></span>
    <%= endnote.page_panel %>:
    <% if (endnote.url) { %>
      <a href="<%= endnote.url %>" target="_blank"><%= endnote.text %></a>
    <% } else { %>
      <%= endnote.text %>
    <% } %>
    </li>
  </script>
  <!-- end endnotes template -->

  <!-- Page template -->
  <script type="text/template" id="page-template">
    <div id="page-container-<%= number %>"  class="page-container<%= typeof gutterBorder !== 'undefined' && gutterBorder === false ? ' no-gutter-border' : '' %>">
      <div class="page-mask">
        <div class="page" id="page-<%= number %>" data-length="<% print(hotspots.length) %>" >
          <% for (var i = 0; i < hotspots.length; i++ ) {%>
            <div class="hotspot" id="hotspot-<%= number %>-<% print(i + 1) %>" data-hotspot-id="<%= number %>-<% print(i + 1) %>" style="<%= hotspots[i] %>"></div>
          <% } %>
          <!-- Dont lazy load the first image -->
          <div class="hover-mask">
            <% if (page_text.length) { %>
              <div class="paragraph-text">
              <% for (var q = 0; q < page_text.length; q++) { %>
                <p><%= page_text[q] %></p>
              <% } %>
              </div>
            <% } %>
            <% if (number == 1) { %>
                <img src="<?php echo $comic_url; ?>/imgs/pages/page-1.<%= img_format %>" alt="<%= script_text %>"/>
            <% } else { %>
              <img src="data:image/gif;base64,R0lGODlhQQLuAuZRAP///wAAAAQCBICAgEBAQL+/vxAQEJ+fn+/v72BgYDAwMHBwcN/f3/z+/M/PzyAgIK+vr1BQUI+Pj/z6/AQGBBQWFPT29Nza3MTGxCQmJFxeXIyOjAwKDOzq7HR2dExKTOTi5Ly6vKSmpPTy9MTCxNze3Ozu7LS2tGRmZGxqbJSWlLSytDQ2NOTm5BQSFHx+fAwODDw+PBwaHMzKzNTS1BweHDQyNFRSVFxaXJyenJSSlERCRFRWVISGhCQiJISChGxubDw6PCwuLHRydERGRKyqrKSipKyurMzOzHx6fExOTLy+vJyanNTW1CwqLIyKjGRiZP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCMTgxMzM0ODBBM0UxMUU0QkEwRjlDRjVGQjQ2QzBDRCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCMTgxMzM0OTBBM0UxMUU0QkEwRjlDRjVGQjQ2QzBDRCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjJDOTE2ODc1MEEzQzExRTRCQTBGOUNGNUZCNDZDMENEIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjJDOTE2ODc2MEEzQzExRTRCQTBGOUNGNUZCNDZDMENEIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEBQYAUQAsAAAAAEEC7gIAB/+AUYKDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8vb6/wMHCw8TFxsfIycrLzM3Oz9DR0tPU1dbX2Nna29zd3t/g4eLj5OXm5+jp6uvs7e7v8PHy8/T19vf4+fr7/P3+/wADChxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izat3KtavXr2DDih1LtqzZs2jTql3Ltq3bt3D/48qdS7eu3bt48+rdy7ev37+AAwseTLiw4cOIEytezLix48eQI0ueTLmy5cuYM2vezLmz58+gQ4seTbq06dOoU6tezbq169ewY8ueTbu27du4c+vezbu379/AgwsfTry48ePIkytfzry58+fQo0ufTr269evYs2vfzr279+/gw4sfT768+fPo06tfz769+/fw48ufT7++/fv48+vfz7+///8ABijggAQWaOCBCCao4IIMNujggxBGKOGEFFZo4YUYZqjhhhx26OGHIIYo4ogklmjiiSimqOKKLLbo4oswxijjjDTWaOONOOao44489ujjj0AGKeSQRBZp5JFIJqnk/5JMNunkk1BGKeWUVFZp5ZVYZqnlllx26eWXYIYp5phklmnmmWimqeaabLbp5ptwxinnnHTWaeedeOap55589unnn4AGKuighBZq6KGIJqrooow26uijkEYq6aSUVmrppZhmqummnHbq6aeghirqqKSWauqpqKaq6qqsturqq7DGKuustNZq66245qrrrrz26uuvwAYr7LDEFmvsscgmq+yyzDbr7LPQRivttNRWa+212Gar7bbcduvtt+CGK+645JZr7rnopqvuuuy26+678MYr77z01mvvvfjmq+++/Pbr778AByzwwB0CYPDBCCes8MIIh8rwwxAn7HDEFDM8cf/FGB98scEmdODxxx43cLDIFG8MAAkUCKDyygKQDEASQegwAsQnEkDAAbHY7IAgBxCwgC8Ko8yyyi4noXIFRrgssYkBBDBALE0XIMgAARAAdNAps+yyByxrMMHCJzb9NCxR8+zz1QmfkPXKRQ/Nw9dLlyg21AFILYzCJcAwdMsHg/DC2gL0oHDYTtNtdzAJN/ABy1krDcAFGaxMwQVxkzg3IwVEYEDTEUBwCAMJPND0AwkcTsgBCjStAM5lR9Hzz4IscHMUA6QegAKeFwL65qQj8HooCWPA8gYhcMB3wiVUsLIGlY94uSILNC190wkUUsDm0zeNMyEJZB9A93VPXfX/IAQEsIDt0+cuiAPYS29A91aDkjAUKwchMgkwOG6wCJLPrDHThVPEAcTGgCggQAKbg50gRKcAuznAdggYhATEFsEHSs9uVItfFMoXAANIIIIFSN0DBoGAzRFgZwfEngY9gbAGyGBlITgYCfQHgAb4YGUraBgAx4aIEgZwEBBoWgGj4IDWCYIBRtycBAiBANGFj3bjEwQHTRfEAAwxeiMkRBVX2AmEdUBycIsYEFb2Ah3K7YeHGGAAIlgI0S0xCgwYAA8H0boBGsAQasRgFDd4O0O0bnPbI4TouMgJhM1gZTbAmApWhgIzWg6NhogeIaPQvQj0sABUe2L0LKm71mWQ/3yQjELZkGjFSO6RhQcTnsoSWbEcMNKRzgslIco3x0F8chAHiAD6LihFWXpyj7T0Y/gK0LRD3PITCAPByipAQ4UZTWUegKWInneIYBrilgjYpQIikMde1lIQv9SgNQlRNmIGwJin7OLBJrA2GjCsAwgLwsqMIM0QUdMQ4+ReADgJvgHsjI5PLJ8CSRhOUH6znE1joz4nqYmELU5ljVzYEIpgMCSwDAT1BNE9CxE9BRzCjYLY3EGfSDWPFqKKehRnKFvXNPUNwoTAQxj/VoaBhaVAAEWYAAvq17xpyhKIQrReUEX5RFy2ropDHAT4UmpQYdotdVycYDoLibAJRO5o7v9M2E0FsFMY9tSe3yuAWMc61gXeTqHZ3KPoqjeI6xmRgQpVI0mBudInVjECBYyj9DQYxwEklXYDMN0BBhDIRCjsBCzjwAbCCICtsuwGjiOc96a3vs29bwALsGxSM0kAzKZOAanjZmUD8IAFDCACVSuoN51qS+8ZQJJtNSJR51g+hhJiYc9cWQZc5liV+cAEgwPgZKU3CAc4ca//VGr2EoAAzhb3uFVDgGr5OFLTZa5p72vuHs1pOmrWlhELa8AYl8nboW0AbDfC5AAkkNxCOECOB0jqYE0XBQjIsb2gqGRMFdaAHOhNAMw8WG9VRtGv2kl039wExEDggQrIoLx7K/D//+SkxEKo8a/qhNgEZoAwDBghBx8OsQgYS6fomU+sEADfQFGZsYzRCQGo9R5zRdHiFtvpvTbrLH6RWWOMmazHDyOYkIdM5CIb+chITrKSl8zkJjv5yVCOspSnTOUqW/nKWM6ylrfM5S57+ctgDrOYx0zmMpv5zGhOs5rXzOY2u/nNcI6znOdM5zrb+c54zrOe98znPvv5z4AOtKAHTehCG/rQiE60ohfN6EY7+tGQjrSkJ03pSlv60pjOtKY3zelOe/rToA61qEdN6lKb+tSoTrWqV83qVrv61bCOtaxnTeta2/rWuM61rnfN6177+tfADrawh03sYhv72MhOtrKXss3sZjv72dCOtrSnTe1qW/va2M62trfN7W57+9vgDre4x03ucpv73OhOt7rXze52u/vd8I63vOdN73rb+974zre+983vfvv73wAPuMAHTvCCG/zgCE+4whfO8IY7/OEQj7jEJ07xilv84hjPuMY3zvGOe/zjIA+5yEdO8pKb/OQoT7nKV87ylrv85TCPucxnTvOa2/zmOM+5znfO8577/OdAD7rQh070ohv96EhPutLpHQgAIfkEBQYAUQAsQAFoARgAHgAAB+qAUYKDNBaDh4iJhyEuQBeKkIgNPgICO0cTkYMAnJ0blZUyPR2anZwNR4KgAkidkJ0XO4OgLA2dpIoAJxyHoEamspq9AjAWnU2RvINEQ5UeUZ0pwoIqDRYuAiWDDTbT0JweRIggwsGcFyfeFYPpgqfeg8rwikLz9opMh9r3L4jykQDuHWqAgZNAQTOE2Ap4KFgUH/oOQRFAAoCFG4faJQJh0AQFATwwsDikBJISU58qzVviipLKQRn2JTqyEECIVYN4mDCYq5MSnEIqmgLYIAWHVURMudJkQkcGUI8ENlhyQ8CQg4Na6GiAKBAAIfkEBQYAUQAsQQFnARcAIAAAB9+AUYKDhFE0hYiFAACEGk+JkIuLglACj5CKkosaApaYmQ0XITudnp+CSzwwhKWXp4WlnUeMr4OxHw2TpxVBFaU3E5qfJLmcAh/BkhODRIiSnDcWmi02iTqDm9GCix0Zr4tFy4Mj3oIWn7SFNbXs7Ykpgyzu84RJhBSDTbXygiKEKoVKBCM0AxKKXIMu1EhmzmCuSQoFSGMkblAIQRceLoooEUCDKNUG3SCkiaOAH0OEFCqHbWONWO0AWPABc56ImsyifPx0sxSQF0c6pDvVs6OwWj0nSmp389w2XUgrQgoEACH5BAUGAFEALEABZwEYACAAAAf0gFGCg4SCAIWIiYUAh4MjipCMjRY4kIqSh0wCF5aDE4InSCMADTYCQ52CLoUsQwICLhaQMZ2vOYkrqa8CLA2FuYkxGYK7r0idGJ+MJRzFp6mGACfFHycTjSSEKosAH7AeJZiCGoO0iyUxRrKYjFEyg0uJE77Rkg0t0IsNJDg9+YUzhCRS8m9QCkFKljRoVDAKCHEFjURhJ2iCCCbtOrErkaSCAAwZKxIyx6hBCCW7bCxkaKmED2cYQ1qaIMOZCw0//EH7USwVCEQgdjUkxONVjQqRIC15dWQCCR0ehvzI1yBDjWsQ8+mYOlFSwRH46g0lJDNRIAAh+QQFBgBRACxAAWgBGAAeAAAH1YBRgoOEhYMAhomKUQCIi4+CjY6QhDORkpAhUTCGOiOYLSeUhS45DQAdGTqjhhogGQIbkDFRToYUArGCF5yLLT8UhLmyo40XGVG5w6yNLbDKupSNqdDLOxlOGSSJACY7FTIV4uBMkpiKDenpUenmjczu5vDx78XyGjj4+jiaij1AQ4DQsFAtV4pTowhWO1iPkAVDCpVRmHEOUsRBHDZMgMRD0EVFQBJ18GgwJCuHAlJYaJCj10kLBxkBaOHBRaIPSwytk1lPlocoK1pIq3jy0KSiRpEGAgAh+QQFBgBRACw9AWsBHgAYAAAH+YBRgoOEg02CSIWKi4yNgwAAjpKKkJGTlxOVjgKKQYw3DZCMJi6EO1ENhC8chD2ilAAtPTWXghQXAA1IRYSVuYeMACAVgzg5QQInvb6WsAAihAICPqmDmZqLlQ1H0t0CPYQnNT8tkh09Pt7SFB2ESus4JNWKIC8V6gIahCX4QiojjCaIiOENA6Ek+ATU0DFvEQ0UMGzMs3DPG4sj1xpVGgFMkAhphFD4CsZsUANkQlAQwvBKUYNQ2ARd0DCjwYQMgyrQYGQhShAjGU0KgqSsUM9GLo40WwbghSIcL55EsaHIBMlcQ2pJGnkJx9aWgogtIjEJLKMjVh0FAgAh+QQFBgBRACw8AWsBIAAYAAAH4YBRgoOEhVEhGIaKi4yEG42NDR+QlIYAACEwlZuXl5mCMZuMnZcYJJA/kBcyG6QAop2FTTICAq2vmxqkhKu1tY+iFCOxgrO+vsCLAA0+gyuXg73HyIqkQIMv0FEXPhQUAoK13hQqlp06gym4UQ3tDaeCQu4NhiBKHztCgzL4H+iCAEoMqtBgV6EV36bV+mABIIAJFAbR0KaoSMJjDAd1miRqxbSMDi+JgAXA4sKGGolVImURpEZCKwhFNNTCFQmUogj1GOIkShFXOSH9pBiUEZJ1ojpEcbEoUVFCGMoJKlIpEAAh+QQFBgBRACw8AWsBIAAYAAAH5oBRgoOEgw0XUSszhYyNjACQAESOlI4NGA2RK5WcgiM6TgIYkRMyjTicDTQoHAKuOJEAKpOdgxMiQa66AhQdkbWFDSFKu7o9wJUlqFG6NQ3IjZAag7qb0ISQDaaCG6wflTWUkS2DHBMAJioj11Gxi4I2sZUqgyjYkRiD8b+FsUz18gaBAEbjHAAPgzwEvKZCxo8WQQYZ4cduBAxXhEBAYkcoxa4oNUYA4EgISTEBHFDQeMZuY4yTAljQYJgJUpFdFDTMYOkIYSMkpCoIqLHBBElBUGKJCMETWgVCHExQPKpDUA+NnQIBADs=" alt="<%= script_text %>"/>
            <% } %>
          </div>
        </div>
      </div>
    </div>
  </script>
  <!-- end page template -->

  <!-- JavaScripts -->
	<?php if ( file_exists( plugin_dir_path( __FILE__ ) . '/js/main.pkgd.min.js' ) ) : ?>
		<script src="<?php echo plugin_dir_url( __FILE__ ) . 'js/main.pkgd.min.js' ?>"></script>
	<?php endif;?>

 	</body>
</html>


<?php endwhile; // End of the loop.
