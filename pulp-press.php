<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width">
	<title><?php _e( "Pulp Press - A web comic maker", 'pulp-comic' ); ?></title>

	<!-- Styles are integrated in pulp-comic.php with safely_add_stylesheet_to_admin()  -->

</head>
<body>
	<div id="page-wrapper">

		<div id="rouge-vif-logo" class="header-item"><?php _e( "Pulp press <span>&mdash; the comic maker companion to Pulp.", 'pulp-comic' ); ?></span></div>

		<div id="steps-container">

			<div class="step-item" data-which="load-data">
				<div class="step-text">
					<div class="step-number"><?php _e( "Optional:", 'pulp-comic' ); ?></div>
					<div class="step-prompt"><?php _e( "Load a previous `pages.json` file.", 'pulp-comic' ); ?></div>
				</div>
				<input id="existing-data" type="file" class="input-btn" multiple>
			</div>

			<div class="step-item" data-which="choose-images">
				<div class="step-text">
					<div class="step-number"><?php _e( "Step One:", 'pulp-comic' ); ?></div>
					<div class="step-prompt"><?php _e( "Choose images, draw hotspots.", 'pulp-comic' ); ?></div>
				</div>
				<input id="images" type="file" class="input-btn" multiple>
			</div>

			<div class="step-item" data-which="add-endnotes">
				<div class="step-text">
					<div class="step-number"><?php _e( "Step Two:", 'pulp-comic' ); ?></div>
					<div class="step-prompt"><?php _e( "Add ", 'pulp-comic' ); ?><span class="internal-link" data-which="endnotes"><?php _e( "endnotes", 'pulp-comic' ); ?></span>.</div>
				</div>
			</div>

			<div class="step-item" data-which="download">
				<div class="step-text">
					<div class="step-number"><?php _e( "Step Three:", 'pulp-comic' ); ?></div>
					<div class="step-prompt"><?php _e( "Get your `pages.json` file.", 'pulp-comic' ); ?></div>
				</div>
				<a id="download-button" href="" download="pages.json">
					<button><?php _e( "Download data", 'pulp-comic' ); ?></button>
				</a>
			</div>

		</div>

		<div id="tabs">
			<ul>
				<li data-which="pages" class="active"><?php _e( "Pages", 'pulp-comic' ); ?></li>
				<li data-which="endnotes"><?php _e( "Endnotes", 'pulp-comic' ); ?></li>
			</ul>
		</div>



		<div data-which="pages" class="main-container-el active">
		</div>

		<div data-which="endnotes" class="notes-list-container main-container-el">
			<button id="add-endnote" data-mode="note" class="add"><?php _e( "+ Add endnote", 'pulp-comic' ); ?></button>
			<ul id="endnotes-container" class="notes-list-container"></ul>
		</div>

	</div>

	<script type="text/jst" id="page-container-templ">
		<div class="page-container" data-hotspots="0" data-page-number="<%= extractPageNumber(fileName) %>">
			<div class="page-furniture">
				<div class="page-info">
					<div class="page-name"><%= fileName %></div>
					<div class="page-actions rail-item">
						<div class="destroy"><button><?php _e( "Delete", 'pulp-comic' ); ?></button></div>
						<div class="save-page"><button><?php _e( "Save page", 'pulp-comic' ); ?></button></div>
					</div>
					<div class="alt-text rail-item">
						<div class="page-info-hed"></div>
						<textarea placeholder="<?php _e( "Panel dialog and captions...", 'pulp-comic' ); ?>"><%= script_text %></textarea>
					</div>
					<div id="footnotes" class="rail-item notes-wrapper">
						<div class="page-info-hed"></div>
						<div id="footnotes-container" class="notes-list-container">
						<% page_text.forEach(function(pageText){ %>
							<div class="note-group">
								<div class="destroy"><span class="text">&times;</span></div><input type="text" name="text" placeholder="<?php _e( "Text...", 'pulp-comic' ); ?>" value="<%= pageText %>">
							</div>
						<% }) %>
						</div>
						<button class="add" data-mode="text">+</button>
					</div>
				</div>
			</div>
			<div class="hotspots"></div>
		</div>
	</script>

	<script type="text/jst" id="hotspot-templ">
		<div class="hotspot create-dragging" data-number="<%= hotspot_number %>">
			<div class="destroy"><span class="text">X</span></div>
			<div class="hotspot-number"><%= hotspot_number %></div>
		</div>
	</script>

	<script type="text/jst" id="note-templ">
		<div class="note-group">
			<div class="destroy"><span class="text">&times;</span></div>
			<input type="text" name="page-panel" placeholder="<?php _e( "Page/panel e.g. p. 1, panel 4", 'pulp-comic' ); ?>"> <input type="text" name="text" placeholder="<?php _e( "Text...", 'pulp-comic' ); ?>"> <input type="text" name="url" placeholder="<?php _e( "URL...", 'pulp-comic' ); ?>">
		</div>
	</script>

	<script type="text/jst" id="text-templ">
		<div class="note-group">
			<div class="destroy"><span class="text">&times;</span></div><input type="text" name="text" placeholder="<?php _e( "Text...", 'pulp-comic' ); ?>">
		</div>
	</script>

	<script src="<?php echo plugin_dir_url( __FILE__ ) . 'pulp-press/js/thirdparty/jquery-1.10.2.js'; ?>"></script>
	<script src="<?php echo plugin_dir_url( __FILE__ ) . 'pulp-press/js/thirdparty/jquery-ui-1.10.4.custom.min.js'; ?>"></script>
	<script src="<?php echo plugin_dir_url( __FILE__ ) . 'pulp-press/js/thirdparty/underscore-min.js'; ?>"></script>
	<script src="<?php echo plugin_dir_url( __FILE__ ) . 'pulp-press/js/app.js'; ?>"></script>

</body>
</html>
