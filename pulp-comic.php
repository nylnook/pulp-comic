<?php
/**
* Plugin Name: Pulp Comic
* Plugin URI: https://framagit.org/nylnook/pulp-comic
* Description: A vivacious comic viewer and manager based on Pulp by Michael Keller
* Version: 0.1 alpha
* Author: nylnook
* Author URI: https://nylnook.art/
* License: MIT
*/

/***
**** Reference documentation
https://developer.wordpress.org/plugins/
***/


/***
**** Security
***/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
     die;
}

/***
**** Translation
***/

// Load translations
function pulp_comic_load_textdomain() {
  	load_plugin_textdomain( 'pulp-comic', false, dirname( plugin_basename(__FILE__) ) . '/languages/' );
}
add_action('plugins_loaded', 'pulp_comic_load_textdomain');


/***
**** Path and URL to Pulp Comic upload directory
***/

// Get default upload directory path
function get_pulp_default_uploads_directory_path() {
    $wp_upload_dir = wp_upload_dir();
    $wp_upload_dir = trailingslashit( $wp_upload_dir['basedir'] );
    $pulp_upload_dir = $wp_upload_dir . 'pulp-comic/';
    return $pulp_upload_dir;
}

// Get default upload directory URL
function get_pulp_default_uploads_directory_url() {
    $wp_upload_url = wp_upload_dir();
    $wp_upload_url = trailingslashit( $wp_upload_url['url'] );
    $pulp_upload_url = $wp_upload_url . 'pulp-comic/';
    return $pulp_upload_url;
}

// deduce FTP path from URL
function pulp_comic_get_file_path_from_url( $url ){
   return $_SERVER['DOCUMENT_ROOT'] . parse_url( $url, PHP_URL_PATH );
}

/***
**** Settings, using the Settings API
https://wpshout.com/making-an-admin-options-page-with-the-wordpress-settings-api/
https://code.tutsplus.com/series/the-complete-guide-to-the-wordpress-settings-api--cms-624
***/

// Initiate setting page
function pulp_comic_settings_init(){
  // Create Setting
    $settings_group = 'pulp-comic';
    $setting_name = 'pulp_comic_settings';
    register_setting( $settings_group, $setting_name, 'pulp_comic_validate_input' );
  // Create Setup Section
    $settings_section_setup = 'pulp_comic_section_setup';
    $page = $settings_group;
    add_settings_section(
        $settings_section_setup,
        __( '<hr>Setup', 'pulp-comic' ),
        'pulp_comic_section_setup_explanation',
        $page
    );
  // Create Social Nextworks Section
    $settings_section_social = 'pulp_comic_section_social';
    add_settings_section(
        $settings_section_social,
        __( '<hr>Social Networks', 'pulp-comic' ),
        'pulp_comic_section_social_explanation',
        $page
    );
  // Create Advanced Settings Section
    $settings_section_advanced = 'pulp_comic_section_advanced';
    add_settings_section(
        $settings_section_advanced,
        __( '<hr>Advanced Settings', 'pulp-comic' ),
        'pulp_comic_section_advanced_explanation',
        $page
    );
  // Add fields to setup section
    add_settings_field(
        'upload_url',
        __('URL to your Pulp comics upload directory', 'pulp-comic' ),
        'pulp_comic_section_setup_upload_url',
        $page,
        $settings_section_setup
    );
  // Add fields to social networks section
    add_settings_field(
        'social_check',
        __('Show Share Buttons for', 'pulp-comic' ),
        'pulp_comic_section_social_input_social_check',
        $page,
        $settings_section_social
    );
    add_settings_field(
        'twitter',
        __('Twitter and Mastodon Settings', 'pulp-comic' ),
        'pulp_comic_section_social_input_twitter',
        $page,
        $settings_section_social
    );
    add_settings_field(
        'facebook',
        __('Facebook Settings', 'pulp-comic' ),
        'pulp_comic_section_social_input_facebook',
        $page,
        $settings_section_social
    );
    add_settings_field(
        'support_check',
        __('Show Support Buttons for', 'pulp-comic' ),
        'pulp_comic_section_social_input_support_check',
        $page,
        $settings_section_social
    );
    add_settings_field(
        'patreon',
        __('Your Patreon Account', 'pulp-comic' ),
        'pulp_comic_section_social_input_patreon',
        $page,
        $settings_section_social
    );
    add_settings_field(
        'tipeee',
        __('Your Tipeee Account', 'pulp-comic' ),
        'pulp_comic_section_social_input_tipeee',
        $page,
        $settings_section_social
    );
    add_settings_field(
        'liberapay',
        __('Your Liberapay Account', 'pulp-comic' ),
        'pulp_comic_section_social_input_liberapay',
        $page,
        $settings_section_social
    );
    // Add fields to advanced section
    add_settings_field(
        'zoom_mode',
        __('Zoom Mode', 'pulp-comic' ),
        'pulp_comic_section_advanced_zoom_mode',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'zoom_options',
        __('Zoom Animation Options', 'pulp-comic' ),
        'pulp_comic_section_advanced_zoom_options',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'lazy_load_extent',
        __('Lazy Load Extent', 'pulp-comic' ),
        'pulp_comic_section_advanced_lazy_load_extent',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'transition_duration',
        __('Transition Duration', 'pulp-comic' ),
        'pulp_comic_section_advanced_transition_duration',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'single_page_width_limit',
        __('Mobile Width Limit', 'pulp-comic' ),
        'pulp_comic_section_advanced_single_page_width_limit',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'drawer_transition_duration',
        __('Mobile Drawer Transition Duration', 'pulp-comic' ),
        'pulp_comic_section_advanced_drawer_transition_duration',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'gutter_width',
        __('Gutter Width', 'pulp-comic' ),
        'pulp_comic_section_advanced_gutter_width',
        $page,
        $settings_section_advanced
    );
    add_settings_field(
        'require_start_on_first_page',
        __('Require Start On First Page', 'pulp-comic' ),
        'pulp_comic_section_advanced_require_start_on_first_page',
        $page,
        $settings_section_advanced
    );
}
add_action( 'admin_init', 'pulp_comic_settings_init' );

// Functions to render text and fields
// Setup section
function pulp_comic_section_setup_explanation(){
    echo '<p>'. __( 'Here are the essentials settings for Pulp Comic: where are your comics pages ?', 'pulp-comic' ). '</p>';
}


function pulp_comic_section_setup_upload_url(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['upload_url'] ) == false) {
      $settings['upload_url'] = get_pulp_default_uploads_directory_url();
      update_option('pulp_comic_settings', $settings);
      // Create folder if it doesn't exist
      $upload_dir = get_pulp_default_uploads_directory_path();
      if (!file_exists($upload_dir)) {
          mkdir($upload_dir, 0755, true);
      };
    };
    $upload_dir = pulp_comic_get_file_path_from_url($settings['upload_url']);
    ?>
    <input class="large-text" type="text" id="upload_url" name="pulp_comic_settings[upload_url]" value="<?php echo esc_attr( $settings['upload_url'] ); ?>">
    <div class="notice notice-info inline">
    	<p><?php _e( 'The corresponding path on your FTP is something like:', 'pulp-comic' ); ?> <br /> <code><?php echo $upload_dir; ?></code></p>
    	<p><em><?php _e( 'Remember you may have to create a corresponding folder and place your comics inside with subfolders with the same slug name than the comic post', 'pulp-comic' ); ?></em></code></p>
    </div>
    <?php
}

// Social Nextworks section
function pulp_comic_section_social_explanation(){
    echo '<p>'. __( 'Choose which buttons to show for the reader to share and support your work', 'pulp-comic' ). '</p>';
}

function pulp_comic_section_social_input_social_check(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['twitter_check'] ) == false) {
      $settings['twitter_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['facebook_check'] ) == false) {
      $settings['facebook_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['gplus_check'] ) == false) {
      $settings['gplus_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['reddit_check'] ) == false) {
      $settings['reddit_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['diaspora_check'] ) == false) {
      $settings['diaspora_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['mastodon_check'] ) == false) {
      $settings['mastodon_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <input type="checkbox" id="twitter_check" name="pulp_comic_settings[twitter_check]" value="1" <?php checked( 1, $settings['twitter_check'], true); ?>>
    <label for="twitter_check"><?php _e( 'Twitter', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="facebook_check" name="pulp_comic_settings[facebook_check]" value="1" <?php checked( 1, $settings['facebook_check'], true); ?>>
    <label for="facebook_check"><?php _e( 'Facebook', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="gplus_check" name="pulp_comic_settings[gplus_check]" value="1" <?php checked( 1, $settings['gplus_check'], true); ?>>
    <label for="gplus_check"><?php _e( 'Google +', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="reddit_check" name="pulp_comic_settings[reddit_check]" value="1" <?php checked( 1, $settings['reddit_check'], true); ?>>
    <label for="reddit_check"><?php _e( 'Reddit', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="reddit_check" name="pulp_comic_settings[diaspora_check]" value="1" <?php checked( 1, $settings['diaspora_check'], true); ?>>
    <label for="diaspora_check"><?php _e( 'diaspora*', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="mastodon_check" name="pulp_comic_settings[mastodon_check]" value="1" <?php checked( 1, $settings['mastodon_check'], true); ?>>
    <label for="mastodon_check"><?php _e( 'Mastodon', 'pulp-comic' ); ?></label>
    <?php
}

function pulp_comic_section_social_input_twitter(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['twitter_account'] ) == false) {
      $settings['twitter_account'] = __( 'pseudo', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['twitter_text'] ) == false) {
      $settings['twitter_text'] = __( 'Hey! Read this great comic!', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <div>
    <label for="twitter_account"><strong><?php _e( 'Your Twitter account', 'pulp-comic' ); ?></strong></label>
    <input type="text" id="twitter_account" name="pulp_comic_settings[twitter_account]" value="<?php echo esc_attr( $settings['twitter_account'] ); ?>">
    </div>
    <div>
    <label for="twitter_text" style="vertical-align: top;"><strong><?php _e( 'Default text to share', 'pulp-comic' ); ?></strong></label>
    <textarea type="text" id="twitter_text" name="pulp_comic_settings[twitter_text]" rows="3" cols="50"><?php echo esc_attr( $settings['twitter_text'] ); ?></textarea>
    </div>
    <?php
}

function pulp_comic_section_social_input_facebook(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['fb_app_id'] ) == false) {
      $settings['fb_app_id'] = __( '16 digit number', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['fb_text'] ) == false) {
      $settings['fb_text'] = __( 'Hey! Read this great comic!', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <div>
    <label for="fb_app_id"><strong><?php _e( 'Your Facebook App ID', 'pulp-comic' ); ?></strong></label>
    <input type="text" id="fb_app_id" name="pulp_comic_settings[fb_app_id]" value="<?php echo esc_attr( $settings['fb_app_id'] ); ?>">
    <label for="fb_app_id"><em><?php _e( 'Ask for it there: ', 'pulp-comic' ); ?><a href="https://developers.facebook.com/">https://developers.facebook.com/</a></em></label>
    </div>
    <div>
    <label for="fb_text" style="vertical-align: top;"><strong><?php _e( 'Default text to share', 'pulp-comic' ); ?></strong></label>
    <textarea type="text" id="fb_text" name="pulp_comic_settings[fb_text]" rows="3" cols="50"><?php echo esc_attr( $settings['fb_text'] ); ?></textarea>
    </div>
    <?php
}

function pulp_comic_section_social_input_support_check(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['patreon_check'] ) == false) {
      $settings['patreon_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['tipeee_check'] ) == false) {
      $settings['tipeee_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['liberapay_check'] ) == false) {
      $settings['liberapay_check'] = '0';
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <input type="checkbox" id="patreon_check" name="pulp_comic_settings[patreon_check]" value="1" <?php checked( 1, $settings['patreon_check'], true); ?>>
    <label for="patreon_check"><?php _e( 'Patreon', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="tipeee_check" name="pulp_comic_settings[tipeee_check]" value="1" <?php checked( 1, $settings['tipeee_check'], true); ?>>
    <label for="tipeee_check"><?php _e( 'Tipeee', 'pulp-comic' ); ?></label>
    <input type="checkbox" id="liberapay_check" name="pulp_comic_settings[liberapay_check]" value="1" <?php checked( 1, $settings['liberapay_check'], true); ?>>
    <label for="liberapay_check"><?php _e( 'Liberapay', 'pulp-comic' ); ?></label>
    <?php
}

function pulp_comic_section_social_input_patreon(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['patreon_account'] ) == false) {
      $settings['patreon_account'] = __( 'pseudo', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <input type="text" id="patreon_account" name="pulp_comic_settings[patreon_account]" value="<?php echo esc_attr( $settings['patreon_account'] ); ?>">
    <?php
}

function pulp_comic_section_social_input_tipeee(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['tipeee_account'] ) == false) {
      $settings['tipeee_account'] = __( 'pseudo', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <input type="text" id="tipeee_account" name="pulp_comic_settings[tipeee_account]" value="<?php echo esc_attr( $settings['tipeee_account'] ); ?>">
    <?php
}

function pulp_comic_section_social_input_liberapay(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['liberapay_account'] ) == false) {
      $settings['liberapay_account'] = __( 'pseudo', 'pulp-comic' );
      update_option('pulp_comic_settings', $settings);
    }
    ?>
    <input type="text" id="liberapay_account" name="pulp_comic_settings[liberapay_account]" value="<?php echo esc_attr( $settings['liberapay_account'] ); ?>">
    <?php
}

// Advanced Settings section
function pulp_comic_section_advanced_explanation(){
    echo '<p>'. __( 'No needs to edit this unless something bother you', 'pulp-comic' ). '</p>';
}

function pulp_comic_section_advanced_zoom_mode(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['zoom_mode'] ) == false) {
      $settings['zoom_mode'] = 'desktop-hover';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <select id="pulp_comic_settings[zoom_mode]" name="pulp_comic_settings[zoom_mode]">
      <option value="desktop-hover" <?php selected( $settings['zoom_mode'], 'desktop-hover'); ?>><?php _e( 'Desktop', 'pulp-comic' ); ?></option>
      <option value="false" <?php selected( $settings['zoom_mode'], 'false'); ?>><?php _e( 'Disabled', 'pulp-comic' ); ?></option>
    </select>
    <label for="pulp_comic_meta_fields[zoom_mode]"><em><?php _e( 'If the zoom toolbar button is clicked, users will zoom into the page on hover following animation settings below', 'pulp-comic' ); ?></em></label>
    <?php
}

function pulp_comic_section_advanced_zoom_options(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['zoom_scale'] ) == false) {
      $settings['zoom_scale'] = '1.5';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_fit'] ) == false) {
      $settings['zoom_fit'] = '1';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_padding'] ) == false) {
      $settings['zoom_padding'] = '0.25';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_in_delay'] ) == false) {
      $settings['zoom_in_delay'] = '200ms';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_out_delay'] ) == false) {
      $settings['zoom_out_delay'] = '1500ms';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_in_speed'] ) == false) {
      $settings['zoom_in_speed'] = '600ms';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_out_speed'] ) == false) {
      $settings['zoom_out_speed'] = '500ms';
      update_option('pulp_comic_settings', $settings);
    }
    if (isset( $settings['zoom_mouse_follow_speed'] ) == false) {
      $settings['zoom_mouse_follow_speed'] = '350ms';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <div>
    <label for="pulp_comic_meta_fields[zoom_scale]"><strong><?php _e( 'Scale', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_scale]" name="pulp_comic_settings[zoom_scale]" value="<?php echo esc_attr( $settings['zoom_scale'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_scale]"><em><?php _e( 'How much you want it to zoom', 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_fit]"><strong><?php _e( 'Fit', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_fit]" name="pulp_comic_settings[zoom_fit]" value="<?php echo esc_attr( $settings['zoom_fit'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_fit]"><em><?php _e( 'A value between 0 and 1. Set this to something around .96 if you want to cut off the edges a little bit. This setting is useful if you have white space around your panels', 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_padding]"><strong><?php _e( 'Padding', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_padding]" name="pulp_comic_settings[zoom_padding]" value="<?php echo esc_attr( $settings['zoom_padding'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_padding]"><em><?php _e( "A value between 0 and .5. Sometimes you don't want the mouse to have to reach the edge of the page to fully zoom. Setting this to something like .25 will mean you've reached the edge of the zoomed in image when you're within 25% of the page edge.", 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_in_delay]"><strong><?php _e( 'In Delay', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_in_delay]" name="pulp_comic_settings[zoom_in_delay]" value="<?php echo esc_attr( $settings['zoom_in_delay'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_in_delay]"><em><?php _e( "A delay that will not trigger a zoom when the mouse is quickly passing over the page", 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_out_delay]"><strong><?php _e( 'Out Delay', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_out_delay]" name="pulp_comic_settings[zoom_out_delay]" value="<?php echo esc_attr( $settings['zoom_out_delay'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_out_delay]"><em><?php _e( "How long to wait after the mouse has exited the page to zoom out. This allows users some leeway if they accidentally zoomed out", 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_in_speed]"><strong><?php _e( 'In Speed', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_in_speed]" name="pulp_comic_settings[zoom_in_speed]" value="<?php echo esc_attr( $settings['zoom_in_speed'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_in_speed]"><em><?php _e( "How fast to zoom in", 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_out_speed]"><strong><?php _e( 'Out Speed', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_out_speed]" name="pulp_comic_settings[zoom_out_speed]" value="<?php echo esc_attr( $settings['zoom_out_speed'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_out_speed]"><em><?php _e( "How fast to zoom out", 'pulp-comic' ); ?></em></label>
    </div>

    <div>
    <label for="pulp_comic_meta_fields[zoom_mouse_follow_speed]"><strong><?php _e( 'Mouse Follow Speed', 'pulp-comic' ); ?></strong></label>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[zoom_mouse_follow_speed]" name="pulp_comic_settings[zoom_mouse_follow_speed]" value="<?php echo esc_attr( $settings['zoom_mouse_follow_speed'] ); ?>">
    <label for="pulp_comic_meta_fields[zoom_mouse_follow_speed]"><em><?php _e( "How fast to follow the mouse", 'pulp-comic' ); ?></em></label>
    </div>

    <?php
}

function pulp_comic_section_advanced_lazy_load_extent(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['lazy_load_extent'] ) == false) {
      $settings['lazy_load_extent'] = '6';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <div>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[lazy_load_extent]" name="pulp_comic_settings[lazy_load_extent]" value="<?php echo esc_attr( $settings['lazy_load_extent'] ); ?>">
    <label for="pulp_comic_meta_fields[lazy_load_extent]"><em><?php _e( 'How many pages behind and ahead do you want to load your images', 'pulp-comic' ); ?></em></label>
    </div>

    <?php
}

function pulp_comic_section_advanced_transition_duration(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['transition_duration'] ) == false) {
      $settings['transition_duration'] = '400';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <div>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[transition_duration]" name="pulp_comic_settings[transition_duration]" value="<?php echo esc_attr( $settings['transition_duration'] ); ?>">
    <label for="pulp_comic_meta_fields[transition_duration]"><em><?php _e( 'In milliseconds, how fast the panels zooms and page turns animate.', 'pulp-comic' ); ?></em></label>
    </div>

    <?php
}

function pulp_comic_section_advanced_single_page_width_limit(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['single_page_width_limit'] ) == false) {
      $settings['single_page_width_limit'] = '500';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <div>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[single_page_width_limit]" name="pulp_comic_settings[single_page_width_limit]" value="<?php echo esc_attr( $settings['single_page_width_limit'] ); ?>">
    <label for="pulp_comic_meta_fields[single_page_width_limit]"><em><?php _e( 'In pixels. A bit of a magic number here to ensure that we go into mobile mode below this value.', 'pulp-comic' ); ?></em></label>
    </div>

    <?php
}

function pulp_comic_section_advanced_drawer_transition_duration(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['drawer_transition_duration'] ) == false) {
      $settings['drawer_transition_duration'] = '500';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <div>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[drawer_transition_duration]" name="pulp_comic_settings[drawer_transition_duration]" value="<?php echo esc_attr( $settings['drawer_transition_duration'] ); ?>">
    <label for="pulp_comic_meta_fields[drawer_transition_duration]"><em><?php _e( 'In milliseconds, how fast the mobile drawer comes in and out.', 'pulp-comic' ); ?></em></label>
    </div>

    <?php
}

function pulp_comic_section_advanced_gutter_width(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['gutter_width'] ) == false) {
      $settings['gutter_width'] = '2px';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <div>
    <input class="small-text" type="text" id="pulp_comic_meta_fields[gutter_width]" name="pulp_comic_settings[gutter_width]" value="<?php echo esc_attr( $settings['gutter_width'] ); ?>">
    <label for="pulp_comic_meta_fields[gutter_width]"><em><?php _e( 'How much space between the two panels in `double` mode. This is the `padding-left` value for `.viewing.right-page`.', 'pulp-comic' ); ?></em></label>
    </div>

    <?php
}

function pulp_comic_section_advanced_require_start_on_first_page(){
    $settings = get_option('pulp_comic_settings');
    if (isset( $settings['require_start_on_first_page'] ) == false) {
      $settings['require_start_on_first_page'] = 'false';
      update_option('pulp_comic_settings', $settings);
    }
    ?>

    <select id="pulp_comic_settings[require_start_on_first_page]" name="pulp_comic_settings[require_start_on_first_page]">
      <option value="false" <?php selected( $settings['require_start_on_first_page'], 'false'); ?>><?php _e( 'No', 'pulp-comic' ); ?></option>
      <option value="true" <?php selected( $settings['require_start_on_first_page'], 'true'); ?>><?php _e( 'Yes', 'pulp-comic' ); ?></option>
    </select>
    <label for="pulp_comic_meta_fields[require_start_on_first_page]"><em><?php _e( 'On load, will the comic require users to land on the first page? Better to disable this to allow for page-specific links', 'pulp-comic' ); ?></em></label>
    <?php
}

// Function to validate inputs and strip out malicious input
function pulp_comic_validate_input( $input ) {
    // Create our array for storing the validated options
    $output = array();

    // Loop through each of the incoming options
    foreach( $input as $key => $value ) {

        // Check to see if the current option has a value. If so, process it.
        if( isset( $input[$key] ) ) {

            // Strip all HTML and PHP tags and properly handle quoted strings
            $output[$key] = strip_tags( stripslashes( $input[ $key ] ) );

        } // end if

    } // end foreach

    // validate the array processing any additional functions filtered by this action
    $validated = apply_filters( 'pulp_comic_validate_input', $output, $input );

    // Display error if input is invalid
    if ($validated !== $input) {
        $type = 'error';
        $message = __( 'Input was invalid', 'pulp-comic' );
        add_settings_error(
            'pulp-comic',
            esc_attr( 'settings_updated' ),
            $message,
            $type
        );
    }

    // Finally return validated input
    return $validated;
}

// Create the adminstration page for Pulp Comic
function pulp_comic_admin_page(){
?>

  <div class="wrap">
      <span style="float:left;font-size:50px;" class="dashicons dashicons-book-alt"></span>
      <div style="padding-left:60px;">
        <h2><?php _e( 'Pulp Comic Settings', 'pulp-comic' ) ?></h2>
        <p><em><?php _e( 'A vivacious viewer for web comics', 'pulp-comic' ) ?></em></p>
      </div>
      <?php //settings_errors(); ?>

      <form action="options.php" method="post">
          <?php
          settings_fields( 'pulp-comic' );
          do_settings_sections( 'pulp-comic' );

          // do_settings_fields('pulp-comic', 'pulp_comic_section_setup');
          // do_settings_fields('pulp-comic', 'pulp_comic_section_social');
          // do_settings_fields('pulp-comic', 'pulp_comic_section_advanced');
          ?>
          <hr>
          <?php submit_button(); ?>
      </form>
      <p><em><?php _e( "Pulp is an open-source viewer for displaying comics online, developed for the story <a href='http://projects.aljazeera.com/2014/terms-of-service'>Terms of Service</a> by <a href='https://mhkeller.com/'>Michael Keller</a>. Pulp was forked into <a href='https://github.com/mhkeller/pulp2'>Pulp2</a> to continue development and support after the closure of Al Jazeera America. And  <a href='https://nylnook.art/'>nylnook</a> is integrating it to Wordpress.", 'pulp-comic' ) ?></em></p>
  </div>

<?php
}

// And create the corresponding submenu in the Settings menu
function pulp_comic_admin_menu(){
    add_submenu_page(
      'options-general.php',
      __( 'Pulp Comic Settings', 'pulp-comic' ),
      __( 'Pulp Comic', 'pulp-comic' ),
      'manage_options',
      'pulp-comic',
      'pulp_comic_admin_page'
    );
}
add_action('admin_menu', 'pulp_comic_admin_menu');


/***
**** Custom Post Type
***/

// Register the Pulp Comic Post Type

function register_pulp_comic() {

    $labels = array(
        'name' => __( 'Pulp Comics', 'pulp-comic' ),
        'singular_name' => __( 'Pulp Comic', 'pulp-comic' ),
        'add_new' => __( 'Add', 'pulp-comic' ),
        'add_new_item' => __( 'Add New Comic', 'pulp-comic' ),
        'edit_item' => __( 'Edit Comic', 'pulp-comic' ),
        'new_item' => __( 'New Comic', 'pulp-comic' ),
        'view_item' => __( 'View Comic', 'pulp-comic' ),
        'search_items' => __( 'Search Comics', 'pulp-comic' ),
        'not_found' => __( 'No comics found', 'pulp-comic' ),
        'not_found_in_trash' => __( 'No comics found in Trash', 'pulp-comic' ),
        'parent_item_colon' => __( 'Parent Comic:', 'pulp-comic' ),
        'menu_name' => __( 'Pulp Comics', 'pulp-comic' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' =>  __( 'A comic viewed with Pulp', 'pulp-comic' ),
        'supports' => array( 'title', 'author', 'thumbnail', 'revisions'),
        'show_in_rest' => true,
        'taxonomies' => array( 'pulp_comic_series' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-book-alt',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('slug' => __( 'comic-viewer', 'pulp-comic' )),
        'capability_type' => 'post'
    );

    register_post_type( 'pulp_comic', $args );
}

add_action( 'init', 'register_pulp_comic' );

// Allow Gutenberg editor to work
add_filter('gutenberg_can_edit_post_type', true, 'pulp_comic');

// Flushing Rewrite on Activation
function pulp_comic_rewrite_flush() {
    register_pulp_comic();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'pulp_comic_rewrite_flush' );

// Register Comic Series Taxonomy
function pulp_comic_series_taxonomy() {
    register_taxonomy(
        'pulp_comic_series',
        'pulp_comic',
        array(
            'hierarchical' => true,
            'description' =>  __( 'Group your comics in series', 'pulp-comic' ),
            'label' => __( 'Series', 'pulp-comic' ),
            'query_var' => true,
            'rewrite' => array(
                'slug' => __( 'series', 'pulp-comic' ),
                'hierarchical' => true
            )
        )
    );
}
add_action( 'init', 'pulp_comic_series_taxonomy');


/***
**** Template
***/

// Filter the single_template with our custom pulp comic template
function pulp_comic_template($single) {
    global $post;
    /* Checks for single template by post type */
    if ( $post->post_type == 'pulp_comic' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . 'single-pulp-comic.php' ) ) {
            return plugin_dir_path( __FILE__ ) . 'single-pulp-comic.php';
        }
    }
    return $single;
}
add_filter('single_template', 'pulp_comic_template');

/**
 * Add Pulp press stylesheet to the Wordpress admin
 */
function safely_add_stylesheet_to_admin( $page ) {
    if( 'post.php' != $page ) {
         return;
    }
    wp_enqueue_style( 'pulp-press-typography', plugins_url('pulp-press/css/typography.css', __FILE__) );
    wp_enqueue_style( 'pulp-press-jquery-ui', plugins_url('pulp-press/css/thirdparty/ui-lightness/jquery-ui-1.10.4.custom.min.css', __FILE__) );
    wp_enqueue_style( 'pulp-press-styles', plugins_url('pulp-press/css/styles.css', __FILE__) );
}

add_action( 'admin_enqueue_scripts', 'safely_add_stylesheet_to_admin' );




/***
**** Sidebar for Pulp press inside each post, using Meta Box for Wordpress before v5.0 and Gutenberg’s Sidebar API for Wordpress v5.0+
https://www.codeinwp.com/blog/make-plugin-compatible-with-gutenberg-sidebar-api/
***/

// Register Pulp Press Meta Box
function pulp_press_add_meta_box() {
	add_meta_box(
			'pulp_press_meta_box',
			__( 'Pulp Press', 'pulp-comic' ),
			'pulp_press_metabox_callback',
			'pulp_comic',
			'normal',
			'high',
			'post',
			array(
				'__back_compat_meta_box' => false,
			)
		);
}
add_action( 'add_meta_boxes', 'pulp_press_add_meta_box' );

// Pulp Press Metabox Callback
function pulp_press_metabox_callback( $post ) {
  $pulp_comic_meta = get_post_meta( $post->ID, 'pulp_press_meta', false );
  // if (empty( $pulp_press_meta )) {
  //   $pulp_press_meta[0] = [
  //       "file_format" => "jpg",
  //       "show_endnotes" => "0",
  //   ];
  // }
	?>

  <div class="notice notice-error inline">
  <p><strong><?php _e( 'WIP : Fully Integrate Pulp Press Here to allow easy edit and upload of new comics pages', 'pulp-comic' ) ?></strong></p>
  </div>

  <?php $pulp_press_path = dirname(__FILE__) ."/pulp-press.php";
  include($pulp_press_path); ?>

  <div class="notice notice-warning inline">
  <p><strong><a href="http://ajam.github.io/pulp-press/" target="_blank"><?php _e( "In the meantime use the online version", 'pulp-comic' ); ?></a> <?php _e( "and don't forget to place your comic in your upload directory with the same directory name than this post slug:", 'pulp-comic' ); ?> <em><?php global $post; echo $post->post_name; ?></em></strong></p>
  </div>

	<?php
}

// Save Pulp Press Metabox Datas
function pulp_press_save_postdata( $post_id ) {
  if ( array_key_exists( 'pulp_press_meta_fields', $_POST ) ) {
    update_post_meta( $post_id, 'pulp_press_meta', $_POST['pulp_press_meta_fields'] );
  }
}
add_action( 'save_post', 'pulp_press_save_postdata' );

// Register Pulp Press Meta Field to Rest API
function pulp_press_register_meta() {
  register_meta(
    'post', 'pulp_press_meta', array(
      'sanitize_callback' => 'pulp_comic_validate_input',
      'type'			=> 'string',
      'single'		=> false,
      'show_in_rest'	=> true,
    )
  );
}
add_action( 'init', 'pulp_press_register_meta' );

// Register Pulp Press Metabox to Rest API
function pulp_press_api_posts_meta_field() {
	register_rest_route(
		'pulp-comic/v1', '/update-meta', array(
			'methods'  => 'POST',
			'callback' => 'pulp_press_update_callback',
			'args'     => array(
				'id' => array(
					'sanitize_callback' => 'absint',
				),
			),
		)
	);
}
add_action( 'rest_api_init', 'pulp_press_api_posts_meta_field' );

// Pulp Press REST API Callback for Gutenberg
function pulp_press_update_callback( $data ) {
	return update_post_meta( $data['id'], $data['key'], $data['value'] );
}

// Test File upload
// https://code.tutsplus.com/series/getting-started-with-the-wordpress-media-uploader--cms-666
// https://www.ibenic.com/wordpress-file-upload-with-ajax/

// add_action( 'add_meta_boxes',   'my_test_metabox' );
//
// function my_test_metabox() {
//     add_meta_box( 'my_test_metabox', 'File upload', 'my_test_metabox_out', 'pulp_comic' );
// }
//
// add_action('post_edit_form_tag', 'update_edit_form' );
// function update_edit_form() {
//     echo ' enctype="multipart/form-data"';
// }
//
// function my_test_metabox_out( $post ) {
//
//     $files = get_post_meta( $post->ID, 'my_files', true );
//     if( ! empty( $files ) ) {
//         echo 'Files uploaded:' . "\r\n";
//         foreach( $files as $file ) {
//             echo '<img src="' . $file['url'] . '" width="100" height="100" />';
//         }
//         echo "\r\n";
//     }
//     echo 'Upload files:' . "\r\n";
//     echo '<input type="file" name="my_files[]" multiple />';
//
// }
//
// add_action( 'save_post', 'my_files_save' );
//
// function my_files_save( $post_id ) {
//
//     if( ! isset( $_FILES ) || empty( $_FILES ) || ! isset( $_FILES['my_files'] ) )
//         return;
//
//     if ( ! function_exists( 'wp_handle_upload' ) ) {
//         require_once( ABSPATH . 'wp-admin/includes/file.php' );
//     }
//     $upload_overrides = array( 'test_form' => false );
//
//     $files = $_FILES['my_files'];
//     foreach ($files['name'] as $key => $value) {
//       if ($files['name'][$key]) {
//         $uploadedfile = array(
//             'name'     => $files['name'][$key],
//             'type'     => $files['type'][$key],
//             'tmp_name' => $files['tmp_name'][$key],
//             'error'    => $files['error'][$key],
//             'size'     => $files['size'][$key]
//         );
//         $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
//
//         if ( $movefile && !isset( $movefile['error'] ) ) {
//             $ufiles = get_post_meta( $post_id, 'my_files', true );
//             if( empty( $ufiles ) ) $ufiles = array();
//             $ufiles[] = $movefile;
//             update_post_meta( $post_id, 'my_files', $ufiles );
//
//         }
//       }
//     }
//
// }


/***
**** Sidebar for specific comic settings inside each post, using Meta Box for Wordpress before v5.0 and Gutenberg’s Sidebar API for Wordpress v5.0+
https://www.codeinwp.com/blog/make-plugin-compatible-with-gutenberg-sidebar-api/
***/

// Register Pulp Comic Meta Box
function pulp_comic_add_meta_box() {
	add_meta_box(
			'pulp_comic_meta_box',
			__( 'Pulp Comic', 'pulp-comic' ),
			'pulp_comic_metabox_callback',
			'pulp_comic',
			'normal',
			'high',
			'post',
			array(
				'__back_compat_meta_box' => false,
			)
		);
}
add_action( 'add_meta_boxes', 'pulp_comic_add_meta_box' );

// Pulp Comic Metabox Callback
function pulp_comic_metabox_callback( $post ) {
  $pulp_comic_meta = get_post_meta( $post->ID, 'pulp_comic_meta', false );
  if (empty( $pulp_comic_meta )) {
    $pulp_comic_meta[0] = [
        "file_format" => "jpg",
        "show_endnotes" => "0",
        "description" => "",
        "keywords" => "",
        "presentation_link" => "",
        "google_play_link" => "",
        "ibooks_link" => "",
        "pdf_link" => "",
        "epub_link" => "",
    ];
  }
	?>

  <p><strong><?php _e( 'Comic Settings', 'pulp-comic' ) ?></strong></p>

  <div style="clear:both;">
  <label for="pulp_comic_meta_fields[presentation_link]"><?php _e( 'A linked presentation and comment post (must be an URL starting with http://)', 'pulp-comic' ) ?></label>
  <input class="large-text" type="text" name="pulp_comic_meta_fields[presentation_link]" id="pulp_comic_meta_fields[presentation_link]" value="<?php echo $pulp_comic_meta[0]['presentation_link'] ?>" />
  <p><em><?php _e( 'This will allow to come back directly to presentation post and to show the comment button', 'pulp-comic' ) ?></em></p>
  </div>

  <label for="pulp_comic_meta_fields[file_format]"><?php _e( 'Pages image file format:', 'pulp-comic' ); ?></label>
  <select id="pulp_comic_meta_fields[file_format]" name="pulp_comic_meta_fields[file_format]">
    <option value="jpg" <?php selected( $pulp_comic_meta[0]['file_format'], 'jpg'); ?>>.jpg</option>
    <option value="jpg" <?php selected( $pulp_comic_meta[0]['file_format'], 'jpeg'); ?>>.jpeg</option>
    <option value="png" <?php selected( $pulp_comic_meta[0]['file_format'], 'png'); ?>>.png</option>
    <option value="webp" <?php selected( $pulp_comic_meta[0]['file_format'], 'webp'); ?>>.webp</option>
    <option value="gif" <?php selected( $pulp_comic_meta[0]['file_format'], 'gif'); ?>>.gif</option>
  </select>

  <!-- <div style="float:right;"> -->
  <label for="pulp_comic_meta_fields[show_endnotes]"><?php _e( 'Show Endnotes', 'pulp-comic' ); ?></label>
  <input type="checkbox" id="pulp_comic_meta_fields[show_endnotes]" name="pulp_comic_meta_fields[show_endnotes]" value="1" <?php if(!empty($pulp_comic_meta[0]['show_endnotes'])) { checked( 1, $pulp_comic_meta[0]['show_endnotes'], true); } ?>>
  <!-- </div> -->

  <p><strong><?php _e( 'Metadatas for search engines and social networks', 'pulp-comic' ) ?></strong></p>

  <label for="pulp_comic_meta_fields[description]"><?php _e( 'Description', 'pulp-comic' ) ?></label>
  <textarea type="text" id="pulp_comic_meta_fields[description]" name="pulp_comic_meta_fields[description]" cols="80" rows="10" class="large-text"><?php if(!empty($pulp_comic_meta[0]['description'])) { echo $pulp_comic_meta[0]['description']; }; ?></textarea>

  <label for="pulp_comic_meta_fields[keywords]"><?php _e( 'Keywords (comma separated)', 'pulp-comic' ) ?></label>
	<input class="large-text" type="text" name="pulp_comic_meta_fields[keywords]" id="pulp_comic_meta_fields[keywords]" value="<?php echo $pulp_comic_meta[0]['keywords'] ?>" />

  <p><strong><?php _e( 'Download Links (must be URLs starting with http://)', 'pulp-comic' ) ?></strong></p>

  <label for="pulp_comic_meta_fields[google_play_link]"><?php _e( 'Google Play download', 'pulp-comic' ) ?></label>
  <input class="large-text" type="text" name="pulp_comic_meta_fields[google_play_link]" id="pulp_comic_meta_fields[google_play_link]" value="<?php echo $pulp_comic_meta[0]['google_play_link'] ?>" />

  <label class="large-text" for="pulp_comic_meta_fields[ibooks_link]"><?php _e( 'iBooks download', 'pulp-comic' ) ?></label>
  <input class="large-text" type="text" name="pulp_comic_meta_fields[ibooks_link]" id="pulp_comic_meta_fields[ibooks_link]" value="<?php echo $pulp_comic_meta[0]['ibooks_link'] ?>" />

  <label for="pulp_comic_meta_fields[pdf_link]"><?php _e( 'PDF download', 'pulp-comic' ) ?></label>
  <input class="large-text" type="text" name="pulp_comic_meta_fields[pdf_link]" id="pulp_comic_meta_fields[pdf_link]" value="<?php echo $pulp_comic_meta[0]['pdf_link'] ?>" />

  <label for="pulp_comic_meta_fields[epub_link]"><?php _e( 'ePub download', 'pulp-comic' ) ?></label>
  <input class="large-text" type="text" name="pulp_comic_meta_fields[epub_link]" id="pulp_comic_meta_fields[epub_link]" value="<?php echo $pulp_comic_meta[0]['epub_link'] ?>" />
	<?php
}

// Save Pulp Comic Metabox Datas
function pulp_comic_save_postdata( $post_id ) {
  if ( array_key_exists( 'pulp_comic_meta_fields', $_POST ) ) {
    update_post_meta( $post_id, 'pulp_comic_meta', $_POST['pulp_comic_meta_fields'] );
  }
}
add_action( 'save_post', 'pulp_comic_save_postdata' );

// Register Pulp Comic Meta Field to Rest API
function pulp_comic_register_meta() {
  register_meta(
    'post', 'pulp_comic_meta', array(
      'sanitize_callback' => 'pulp_comic_validate_input',
      'type'			=> 'string',
      'single'		=> false,
      'show_in_rest'	=> true,
    )
  );
}
add_action( 'init', 'pulp_comic_register_meta' );

// Register Pulp Comic Metabox to Rest API
function pulp_comic_api_posts_meta_field() {
	register_rest_route(
		'pulp-comic/v1', '/update-meta', array(
			'methods'  => 'POST',
			'callback' => 'pulp_comic_update_callback',
			'args'     => array(
				'id' => array(
					'sanitize_callback' => 'absint',
				),
			),
		)
	);
}
add_action( 'rest_api_init', 'pulp_comic_api_posts_meta_field' );

// Pulp Comic REST API Callback for Gutenberg
function pulp_comic_update_callback( $data ) {
	return update_post_meta( $data['id'], $data['key'], $data['value'] );
}



?>
